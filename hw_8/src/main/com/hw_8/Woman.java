package main.com.hw_8;

import java.util.Map;

public final class Woman extends Human{


    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule, Pet pet) {
        super(name, surname, year, iq, schedule, pet);
    }
    public Woman() {
    }

    public void makeup(){
        System.out.println("підфарбуватися");
    }

    @Override
    public void greetPet() {
        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                System.out.println("Привіт, " + getFamily().findOnePet(pet).getNickname() + ". Ти дуже гарний!");
            }
        } else {
            System.out.println("no pets");
        }
    }
}
