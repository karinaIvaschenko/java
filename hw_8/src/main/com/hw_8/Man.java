package main.com.hw_8;

import java.util.Map;

public final class Man extends Human{
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule, Pet pet) {
        super(name, surname, year, iq, schedule, pet);
    }

    public Man() {
    }

    public void repairCar(){
        System.out.println("лагодити авто");
    }

    @Override
    public void greetPet() {
        if (getFamily().getPets() != null) {
            for (Pet pet : getFamily().getPets()) {
                System.out.println("Привіт, " + getFamily().findOnePet(pet).getNickname() + ". Ти дуже гарний!!!");
            }
        } else {
            System.out.println("no pets");
        }
    }
}
