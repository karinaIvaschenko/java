package main.com.hw_8.DAO;

import main.com.hw_8.Family;
import main.com.hw_8.Human;
import main.com.hw_8.Pet;

import java.util.List;
import java.util.Set;

public interface FamilyDao {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(int index);
    boolean deleteFamily(Family family);
    void saveFamily (Family family);
}
