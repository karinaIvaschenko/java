package main.com.hw_6;

public final class Woman extends Human{


    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman() {
    }

    public void makeup(){
        System.out.println("підфарбуватися");
    }
    @Override
    public void greetPet()  {
        System.out.println("Привіт, " + this.getFamily().getPet().getNickname());
    }
}
