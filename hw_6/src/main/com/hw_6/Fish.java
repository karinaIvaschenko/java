package main.com.hw_6;

public class Fish extends Pet implements Foul{
    public Fish(String nickname) {
        super(nickname);
    }

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    public Fish() {
    }
    @Override
    public void respond (){
        System.out.println("Привіт, хазяїн. Я - " +this.getNickname()+ ". Я скучив!");
    }
    @Override
    public void foul(){
        System.out.println("Потрібно добре замести сліди...");
    }
}
