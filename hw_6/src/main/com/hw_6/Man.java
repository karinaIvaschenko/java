package main.com.hw_6;

public final class Man extends Human{
    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {
    }

    public void repairCar(){
        System.out.println("лагодити авто");
    }

    @Override
    public void greetPet()  {
        System.out.println("Привіт, " + this.getFamily().getPet().getNickname());
    }
}
