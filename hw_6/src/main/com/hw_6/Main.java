package main.com.hw_6;

public class Main {
    public static void main(String[] args) {
        String[][] schedule = {
                {DayOfWeek.MONDAY.name(), "go home"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "SPA"},
                {DayOfWeek.THURSDAY.name(), "go home"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "film"},
                {DayOfWeek.SUNDAY.name(), "zoo"}

        };

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        System.out.println(mother);

        System.out.println("***********************************");

        Human father = new Human("Max", "Ivaschenko", 1989,100, schedule);
        System.out.println(father);

        System.out.println("***********************************");

        Family family = new Family(mother, father);

        Human child1 = new Human("Eugeniya", "Ivaschenko", 2018,100, schedule);
        System.out.println(child1);

        System.out.println("***********************************");

        Human child2 = new Human("Artur", "Ivaschenko", 2021,100, schedule);
        System.out.println(child2);

        System.out.println("***********************************");

        Human child3 = new Human("Oleg", "Oleg", 2015,100, schedule);
        System.out.println(child3);

        Human child4 = new Human("Child", "Child", 1234, 100, schedule);
        System.out.println(child4);

        System.out.println("***********************************");

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        family.deleteChild(child3);

        family.deleteChildIndex(family.getChildIndex(child4));

        Dog pet = new Dog("Dog", 2, 100,new String[]{"eat", "sleep"});
        pet.setSpecies(Species.BEAGLE);
        System.out.println(pet);

        family.setPet(pet);
        Human child5 = new Human();
        family.bornChild(child5);



        System.out.println("***********************************");

        System.out.println(family);

//
//        for (int i = 0; i < 10000001; i++) {
//            Human human = new Human();
//        }
    }
}
