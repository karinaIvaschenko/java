package test.com.hw_6;

import main.com.hw_6.Dog;
import main.com.hw_6.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void testToString() {
        Dog pet = new Dog("Dog", 2, 100,new String[]{"eat", "sleep"});
        pet.setSpecies(Species.GERMAN_SHEPHERD);
       String petExpect = "GERMAN_SHEPHERD{nickname='Dog', age=2, trickLevel=100, habits=[eat, sleep]canFly=falsenumberOfLegs=4hasFur=true}";
       assertEquals(petExpect, pet.toString(), "Ok");
        Dog pet1 = new Dog("Dog", 5, 100,new String[]{"eat", "sleep"});
        pet1.setSpecies(Species.GERMAN_SHEPHERD);
        assertNotEquals(petExpect, pet1.toString(), "Not Ok");
    }

    @Test
    void testEquals() {
        Dog pet = new Dog("Dog", 2, 100,new String[]{"eat", "sleep"});
        Dog pet1 = new Dog("Dog", 2, 100,new String[]{"eat", "sleep"});
        Dog pet2 = new Dog( "Dog1", 2, 100,new String[]{"eat", "sleep"});
        assertEquals(pet, pet1, "Ok");
        assertNotEquals(pet, pet2, "Not Ok");
    }

    @Test
    void testHashCode() {
        Dog pet = new Dog("Dog", 2, 100,new String[]{"eat", "sleep"});
        Dog pet1 = new Dog("Dog", 2, 100,new String[]{"eat", "sleep"});
        Dog pet2 = new Dog("Dog1", 2, 100,new String[]{"eat", "sleep"});
        assertEquals(pet.hashCode(), pet1.hashCode(), "Ok");
        assertNotEquals(pet.hashCode(), pet2.hashCode(), "Not Ok");
    }
}