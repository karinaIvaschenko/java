package test.com.hw_11;

import main.com.hw_11.Dog;
import main.com.hw_11.Species;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void testToString() {
        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        Dog pet = new Dog("Dog", 2, 100, habits);
        pet.setSpecies(Species.GERMAN_SHEPHERD);
       String petExpect = "GERMAN_SHEPHERD{nickname='Dog', age=2, trickLevel=100, habits=[eat, sleep]canFly=falsenumberOfLegs=4hasFur=true}";
       assertEquals(petExpect, pet.toString(), "Ok");
        Dog pet1 = new Dog("Dog", 5, 100, habits);
        pet1.setSpecies(Species.GERMAN_SHEPHERD);
        assertNotEquals(petExpect, pet1.toString(), "Not Ok");
    }

    @Test
    void testEquals() {
        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        Dog pet = new Dog("Dog", 2, 100, habits);
        Dog pet1 = new Dog("Dog", 2, 100, habits);
        Dog pet2 = new Dog( "Dog1", 2, 100, habits);
        assertEquals(pet, pet1, "Ok");
        assertNotEquals(pet, pet2, "Not Ok");
    }

    @Test
    void testHashCode() {
        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");
        Dog pet = new Dog("Dog", 2, 100, habits);
        Dog pet1 = new Dog("Dog", 2, 100, habits);
        Dog pet2 = new Dog("Dog1", 2, 100, habits);
        assertEquals(pet.hashCode(), pet1.hashCode(), "Ok");
        assertNotEquals(pet.hashCode(), pet2.hashCode(), "Not Ok");
    }
}