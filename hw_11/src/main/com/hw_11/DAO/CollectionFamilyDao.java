package main.com.hw_11.DAO;

import main.com.hw_11.Family;


import java.util.*;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList;

    public CollectionFamilyDao() {
        this.familyList = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        return this.familyList;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.familyList.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < this.familyList.size()) {
            this.familyList.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (familyList.size() > 0){
            this.familyList.remove(family);
            return true;
        }
        return false;
    }

    @Override
    public void saveFamily(Family family) {
        if (this.familyList.contains(family)) {
            int index = familyList.indexOf(family);
            this.familyList.set(index, family);
        } else {
            familyList.add(family);
        }
    }
}