package test.com.hw_9;

import main.com.hw_9.Dog;
import main.com.hw_9.Family;
import main.com.hw_9.Human;
import main.com.hw_9.Pet;
import main.com.hw_9.Services.FamilyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    private FamilyService familyService;
    @BeforeEach
    void init(){
        this.familyService = new FamilyService();
        Human mother = new Human("Kari", "Ivaschenko", 1999);
        Human father = new Human("Max", "Ivaschenko", 1989);
        familyService.createNewFamily(mother, father);
    }
    @Test
    void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    @Test
    void getFamiliesBiggerThan() {
        List<Family> result = familyService.getFamiliesBiggerThan(1);
        assertEquals(1, result.size());
    }

    @Test
    void getFamiliesLessThan() {
        List<Family> result = familyService.getFamiliesLessThan(3);
        assertEquals(1, result.size());
    }

    @Test
    void countFamiliesWithMemberNumber() {
        int result = familyService.countFamiliesWithMemberNumber(2);
        assertEquals(1, result);
    }

    @Test
    void createNewFamily() {
        FamilyService familyService2 = new FamilyService();
        assertNotEquals(familyService, familyService2);
    }

    @Test
    void bornChild() {
        Family familyResult = familyService.bornChild(familyService.getFamilyById(0), "Kar", "Ol");
        assertEquals(1, familyResult.getChildren().size());
    }

    @Test
    void adoptChild() {
        Human child = new Human("Child", "Child", 1234);
        Family familyResult = familyService.adoptChild(familyService.getFamilyById(0), child);
        assertEquals(1, familyResult.getChildren().size());
    }

    @Test
    void deleteAllChildrenOlderThen() {
        Human child = new Human("Child", "Child", 1234);
        Human child2 = new Human("Child2", "Child2", 2000);
        familyService.adoptChild(familyService.getFamilyById(0), child);
        familyService.adoptChild(familyService.getFamilyById(0), child2);
        familyService.deleteAllChildrenOlderThen(18);
        assertEquals(2, familyService.getFamilyById(0).countFamily());
    }

    @Test
    void count() {
        int result = familyService.count();
        assertEquals(1, result);
    }

    @Test
    void getPets() {
        Dog pet = new Dog("Dog");
        Set<Pet> result1 = familyService.getPets(0);
        assertEquals(Collections.EMPTY_SET, result1);
        familyService.getFamilyById(0).addPet(pet);
        Set<Pet> result = familyService.getPets(0);
        assertEquals(1, result.size());
    }

    @Test
    void addPet() {
        Dog pet = new Dog("Dog");
        familyService.addPet(0, pet);
        Set<Pet> result = familyService.getPets(0);
        assertNotEquals(Collections.EMPTY_SET, result);
    }

    @Test
    void deleteFamilyByIndex() {
        Human mother2 = new Human("Kar", "Ivaschenko", 1999);
        Human father2 = new Human("M", "Ivaschenko", 1989);
        familyService.createNewFamily(mother2, father2);
        familyService.deleteFamilyByIndex(0);
        assertEquals(1, familyService.count());
    }

    @Test
    void deleteFamilyByFamily() {
        Human mother2 = new Human("Kar", "Ivaschenko", 1999);
        Human father2 = new Human("M", "Ivaschenko", 1989);
        familyService.createNewFamily(mother2, father2);
        familyService.deleteFamilyByFamily(familyService.getFamilyById(0));
        assertEquals(1, familyService.count());
    }

    @Test
    void getFamilyById() {
        Family familyResult = familyService.getFamilyById(0);
        assertEquals("Kari", familyResult.getMother().getName());
    }
}