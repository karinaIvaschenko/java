package main.com.hw_9;

import main.com.hw_9.Controllers.FamilyController;

import java.text.ParseException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ParseException {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "go home");
        schedule.put(DayOfWeek.TUESDAY, "go to gym");
        schedule.put(DayOfWeek.WEDNESDAY, "SPA");
        schedule.put(DayOfWeek.THURSDAY, "go home");
        schedule.put(DayOfWeek.FRIDAY, "visit friends");
        schedule.put(DayOfWeek.SATURDAY, "film");
        schedule.put(DayOfWeek.SUNDAY, "zoo");

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        System.out.println(mother);

        System.out.println("***********************************");

        Human father = new Human("Max", "Ivaschenko", 1989,100, schedule);
        System.out.println(father);

        System.out.println("***********************************");

        Family family = new Family(mother, father);

        Human child1 = new Human("Eugeniya", "Ivaschenko", 2018,100, schedule);
        System.out.println(child1);

        System.out.println("***********************************");

        Human child2 = new Human("Artur", "Ivaschenko", 2021,100, schedule);
        System.out.println(child2);

        System.out.println("***********************************");

        Human child3 = new Human("Oleg", "Oleg", 2015,100, schedule);
        System.out.println(child3);

        Human child4 = new Human("Child", "Child", 1234, 100, schedule);
        System.out.println(child4);

        System.out.println("***********************************");

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        family.deleteChild(child3);

        family.deleteChildIndex(family.getChildIndex(child4));

        Set<Pet> pets= new HashSet<>();
        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");

        Dog pet = new Dog("Dog", 2, 100, habits);
        pet.setSpecies(Species.BEAGLE);

        Dog pet1 = new Dog("Dog1", 4, 40, habits);

        pets.add(pet);
        pets.add(pet1);
        family.setPets(pets);
        System.out.println(pet);

        child1.greetPet();
        child1.feedPet(true);

        Human child5 = new Human();
        family.bornChild(child5);



        System.out.println("***********************************");

        System.out.println(family);


        System.out.println("----------------------");

        Human mother2 = new Human("Kari", "Ivaschenko", 1999, 100, schedule);

        Human father2 = new Human("Max", "Ivaschenko", 1989,100, schedule);

        Human father2child = new Human("Child", "Child", 2019, 100, schedule);

        Dog pet2 = new Dog("Dog", 2, 100, habits);


        FamilyController familyController = new FamilyController();
        familyController.createNewFamily(mother2, father2);
        familyController.getFamiliesBiggerThan(1);
        familyController.getFamiliesLessThan(1);
        familyController.countFamiliesWithMemberNumber(1);
        familyController.bornChild(familyController.getFamilyById(0), "Kar", "Ol");
        familyController.adoptChild(familyController.getFamilyById(0), father2child);
        familyController.deleteAllChildrenOlderThen(18);
        familyController.count();
        familyController.addPet(0, pet2);
        familyController.displayAllFamilies();
        familyController.getPets(0);
        familyController.getFamilyById(0);
        familyController.deleteFamilyByFamily(familyController.getFamilyById(0));
        familyController.deleteFamilyByIndex(0);

        Human human = new Human("Viktor", "Lyt", "04/05/1989", 100);
        System.out.println(human);



//
//        for (int i = 0; i < 10000001; i++) {
//            Human human = new Human();
//        }
    }
}
