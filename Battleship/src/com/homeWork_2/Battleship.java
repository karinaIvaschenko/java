package com.homeWork_2;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Battleship {
    public static void main(String[] args) {
        System.out.println("All Set. Get ready to rumble!");
        Random random = new Random();
        Scanner scan = new Scanner(System.in);

        char[][] field = new char[5][5];
        char target = '-';

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                field[i][j] = target;
            }
        }

        int horizontallyNum = random.nextInt(field.length);
        int verticallyNum = random.nextInt(field.length);

        String[] directions = {"left","right","up","down"};

        int[][] coords = new int[3][2];

        while(true) {
            int directIndex = random.nextInt(directions.length);

            if(directions[directIndex].equals("left")) {
                if (horizontallyNum - 1 >= 0 && horizontallyNum - 2 >= 0) {
                    coords[0] = new int[]{verticallyNum, horizontallyNum};
                    coords[1] = new int[]{verticallyNum, horizontallyNum - 1};
                    coords[2] = new int[]{verticallyNum, horizontallyNum - 2};
                    break;}
                } else if (directions[directIndex].equals("right")) {
                    if(horizontallyNum  + 1 < field.length && horizontallyNum+2 < field.length){
                        coords[0] = new int[]{verticallyNum, horizontallyNum};
                        coords[1] = new int[]{verticallyNum, horizontallyNum + 1};
                        coords[2] = new int[]{verticallyNum, horizontallyNum + 2};
                        break;
                    }
                }
                else if(directions[directIndex].equals("down")){
                    if(verticallyNum + 1 < field.length && verticallyNum +2 < field.length){
                        coords[0] = new int[]{verticallyNum, horizontallyNum};
                        coords[1] = new int[]{verticallyNum + 1, horizontallyNum};
                        coords[2] = new int[]{verticallyNum + 2, horizontallyNum};
                        break;
                    }
                }
                else if(directions[directIndex].equals("up")){
                    if(verticallyNum - 1 >= 0 && verticallyNum -2 >=0){
                        coords[0] = new int[]{verticallyNum, horizontallyNum};
                        coords[1] = new int[]{verticallyNum - 1, horizontallyNum};
                        coords[2] = new int[]{verticallyNum - 2, horizontallyNum};
                        break;
                    }

            }}

        System.out.println(Arrays.deepToString(coords));

        int counter= 0;
        while (true){
            String header = "0 | 1 | 2 | 3 | 4 | 5 |";
            System.out.println(header);

            for (int i = 0; i < field.length; i++) {
                System.out.print(i+1 + " | ");
                for (int j = 0; j < field.length; j++) {
                    System.out.print(field[i][j] + " | ");
                }
                System.out.println();
            }

            if (counter == 3){
                break;
            }

            System.out.println("Specify the row for the shot (1-5)");
            int row = scan.nextInt();
            System.out.println("Specify the column for the shot (1-5)");
            int column = scan.nextInt();

            for (int[] array:coords) {
                if (row-1 == array[0] && column-1 == array[1]){
                    field[row - 1][column - 1] = 'x';
                    counter ++;
                    break;
                }else {
                    field[row - 1][column - 1] = '*';
                }
            }

        }



        }

}
