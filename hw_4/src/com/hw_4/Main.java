package com.hw_4;

public class Main {
    public static void main(String[] args) {
        Human mother = new Human("Kari", "Ivaschenko", 1999);
        System.out.println(mother);

        Human father = new Human("Max", "Ivaschenko", 1989);
        System.out.println(father);
        Family family = new Family(mother, father);

        Human child1 = new Human("Eugeniya", "Ivaschenko", 2018);
        System.out.println(child1);

        Human child2 = new Human("Artur", "Ivaschenko", 2021);
        System.out.println(child2);

        Human child3 = new Human("Oleg", "Oleg", 2015);
        System.out.println(child3);

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.deleteChild(child3);

        Pet pet = new Pet("Dog", "Dog", 2, 100,new String[]{"eat", "sleep"});
        family.setPet(pet);

        System.out.println(family);
    }
}
