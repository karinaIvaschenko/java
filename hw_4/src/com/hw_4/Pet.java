package com.hw_4;

import java.util.Arrays;

public class Pet {
    static {
        System.out.println("Loading new Class Pet");
    }
    {
        System.out.println("Creating new Pet Object");
    }

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public void eat (){
        System.out.println("Я ї'м!");
    }
    public void respond (){
        System.out.println("Привіт, хазяїн. Я - " +nickname+ ". Я скучив!");
    }

    public void foul (){
        System.out.println("Потрібно добре замести сліди...");
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public  Pet(String species, String nickname){
    this.species = species;
    this.nickname = nickname;
    }

    public  Pet(String species, String nickname, int age, int trickLevel, String[] habits){
       this.species = species;
       this.nickname = nickname;
       this.age = age;
       this.trickLevel = trickLevel;
       this.habits = habits;
    }

    public Pet(){}

    @Override
    public String toString() {
        return this.species+"{nickname='"+this.nickname+"', age="+this.age+", trickLevel="+this.trickLevel+", habits="+Arrays.toString(this.habits)+"}";

    }
}
