package test;

import main.com.hw_5.Pet;
import main.com.hw_5.Species;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void testToString() {
        Pet pet = new Pet(Species.GERMAN_SHEPHERD, "Dog", 2, 100,new String[]{"eat", "sleep"});
       String petExpect = "GERMAN_SHEPHERD{nickname='Dog', age=2, trickLevel=100, habits=[eat, sleep]}";
       assertEquals(petExpect, pet.toString(), "Ok");
        Pet pet1 = new Pet(Species.GERMAN_SHEPHERD, "Dog", 5, 100,new String[]{"eat", "sleep"});
        assertNotEquals(petExpect, pet1.toString(), "Not Ok");
    }

    @Test
    void testEquals() {
        Pet pet = new Pet(Species.GERMAN_SHEPHERD, "Dog", 2, 100,new String[]{"eat", "sleep"});
        Pet pet1 = new Pet(Species.GERMAN_SHEPHERD, "Dog", 2, 100,new String[]{"eat", "sleep"});
        Pet pet2 = new Pet(Species.GERMAN_SHEPHERD, "Dog1", 2, 100,new String[]{"eat", "sleep"});
        assertEquals(pet, pet1, "Ok");
        assertNotEquals(pet, pet2, "Not Ok");
    }

    @Test
    void testHashCode() {
        Pet pet = new Pet(Species.GERMAN_SHEPHERD, "Dog", 2, 100,new String[]{"eat", "sleep"});
        Pet pet1 = new Pet(Species.GERMAN_SHEPHERD, "Dog", 2, 100,new String[]{"eat", "sleep"});
        Pet pet2 = new Pet(Species.GERMAN_SHEPHERD, "Dog1", 2, 100,new String[]{"eat", "sleep"});
        assertEquals(pet.hashCode(), pet1.hashCode(), "Ok");
        assertNotEquals(pet.hashCode(), pet2.hashCode(), "Not Ok");
    }
}