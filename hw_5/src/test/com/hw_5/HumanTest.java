package test;

import main.com.hw_5.DayOfWeek;
import main.com.hw_5.Human;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testToString() {
        String[][] schedule = {
                {DayOfWeek.MONDAY.name(), "go home"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "SPA"},
                {DayOfWeek.THURSDAY.name(), "go home"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "film"},
                {DayOfWeek.SUNDAY.name(), "zoo"}

        };

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        String mother1 = "Human{name= 'Kari', surname= 'Ivaschenko', year= 1999, iq= 100schedule= [[MONDAY, go home], [TUESDAY, go to gym], [WEDNESDAY, SPA], [THURSDAY, go home], [FRIDAY, visit friends], [SATURDAY, film], [SUNDAY, zoo]]}}";
        String mother2 = "Human{name= 'Karina', surname= 'Ivaschenko', year= 1999, iq= 100schedule= [[MONDAY, go home], [TUESDAY, go to gym], [WEDNESDAY, SPA], [THURSDAY, go home], [FRIDAY, visit friends], [SATURDAY, film], [SUNDAY, zoo]]}}";
        assertEquals(mother1, mother.toString(), "Ok");
        assertNotEquals(mother2, mother.toString(), "Not ok");
    }

    @Test
    void testEquals() {
        String[][] schedule = {
                {DayOfWeek.MONDAY.name(), "go home"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "SPA"},
                {DayOfWeek.THURSDAY.name(), "go home"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "film"},
                {DayOfWeek.SUNDAY.name(), "zoo"}

        };

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        Human mother1 = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        Human mother2 = new Human("Karina", "Ivaschenko", 1999, 100, schedule);
        assertEquals(mother, mother1, "Ok");
        assertNotEquals(mother, mother2, "Not Ok");
    }

    @Test
    void testHashCode() {
        String[][] schedule = {
                {DayOfWeek.MONDAY.name(), "go home"},
                {DayOfWeek.TUESDAY.name(), "go to gym"},
                {DayOfWeek.WEDNESDAY.name(), "SPA"},
                {DayOfWeek.THURSDAY.name(), "go home"},
                {DayOfWeek.FRIDAY.name(), "visit friends"},
                {DayOfWeek.SATURDAY.name(), "film"},
                {DayOfWeek.SUNDAY.name(), "zoo"}

        };

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        Human mother1 = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        Human mother2 = new Human("Karina", "Ivaschenko", 1999, 100, schedule);
        assertEquals(mother.hashCode(), mother1.hashCode(), "Ok");
        assertNotEquals(mother.hashCode(), mother2.hashCode(), "Not Ok");
    }
}