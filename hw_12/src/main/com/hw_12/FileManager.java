package main.com.hw_12;

import java.io.*;
import java.util.List;

public class FileManager {
    private static final String data = "Data/data.dat";
    private static final String testData = "Data/test.dat";
 public static void writeData(List<Family> familyList){
     try {
         FileOutputStream fileOutputStream = new FileOutputStream(data);
         ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
         objectOutputStream.writeObject(familyList);
     }catch (IOException e) {
         throw new RuntimeException(e);
     }
 }

 public static List<Family> loadTestData(){
     List<Family> familyList;
     try {
         FileInputStream fileInputStream = new FileInputStream(testData);
         ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

         familyList = (List<Family>) objectInputStream.readObject();

     }catch (IOException | ClassNotFoundException e) {
         throw new RuntimeException(e);
     }
     return familyList;
 }

    public static List<Family> loadData(){
        List<Family> familyList;
        try {
            FileInputStream fileInputStream = new FileInputStream(data);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            familyList = (List<Family>) objectInputStream.readObject();

        }catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return familyList;
    }
}
