package main.com.hw_12;

import main.com.hw_12.Controllers.FamilyController;

import java.text.ParseException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ParseException {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "go home");
        schedule.put(DayOfWeek.TUESDAY, "go to gym");
        schedule.put(DayOfWeek.WEDNESDAY, "SPA");
        schedule.put(DayOfWeek.THURSDAY, "go home");
        schedule.put(DayOfWeek.FRIDAY, "visit friends");
        schedule.put(DayOfWeek.SATURDAY, "film");
        schedule.put(DayOfWeek.SUNDAY, "zoo");

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        System.out.println(mother);

        System.out.println("***********************************");

        Human father = new Human("Max", "Ivaschenko", 1989, 100, schedule);
        System.out.println(father);

        System.out.println("***********************************");

        Family family = new Family(mother, father);

        Human child1 = new Human("Eugeniya", "Ivaschenko", 2018, 100, schedule, "girl");
        System.out.println(child1);

        System.out.println("***********************************");

        Human child2 = new Human("Artur", "Ivaschenko", 2021, 100, schedule, "boy");
        System.out.println(child2);

        System.out.println("***********************************");

        Human child3 = new Human("Oleg", "Oleg", 2015, 100, schedule);
        System.out.println(child3);

        Human child4 = new Human("Child", "Child", 1234, 100, schedule);
        System.out.println(child4);

        System.out.println("***********************************");

        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        family.deleteChild(child3);

        family.deleteChildIndex(family.getChildIndex(child4));

        Set<Pet> pets = new HashSet<>();
        Set<String> habits = new HashSet<>();
        habits.add("eat");
        habits.add("sleep");

        Dog pet = new Dog("Dog", 2, 100, habits);
        pet.setSpecies(Species.BEAGLE);

        Dog pet1 = new Dog("Dog1", 4, 40, habits);

        pets.add(pet);
        pets.add(pet1);
        family.setPets(pets);
        System.out.println(pet);

        child1.greetPet();
        child1.feedPet(true);

        Human child5 = new Human();
        family.bornChild(child5);


        System.out.println("***********************************");

        System.out.println(family);


        System.out.println("----------------------");

        Human mother2 = new Human("Kari", "Ivaschenko", 1999, 100, schedule);

        Human father2 = new Human("Max", "Ivaschenko", 1989, 100, schedule);


        Human mother3 = new Human("Kari", "Ivaschenko", 1999, 100, schedule);

        Human father3 = new Human("Max", "Ivaschenko", 1989, 100, schedule);

        Human father2child = new Human("Child", "Child", 2019, 100, schedule);

        Dog pet2 = new Dog("Dog", 2, 100, habits);


        FamilyController familyController = new FamilyController();
//        familyController.createNewFamily(mother2, father2);
//        familyController.getFamiliesBiggerThan(1);
//        familyController.getFamiliesLessThan(1);
//        familyController.countFamiliesWithMemberNumber(2);
//        familyController.bornChild(familyController.getFamilyById(0), "Kar", "Ol");
//        familyController.adoptChild(familyController.getFamilyById(0), father2child);
//        familyController.deleteAllChildrenOlderThen(18);
//        familyController.count();
//        familyController.addPet(0, pet2);
//        familyController.displayAllFamilies();
//        familyController.getPets(0);
//        familyController.getFamilyById(0);
//        familyController.deleteFamilyByFamily(familyController.getFamilyById(0));
//        familyController.deleteFamilyByIndex(0);

        Human human = new Human("Viktor", "Lyt", "04/05/1989", 100);
        System.out.println(human);

        System.out.println("----------------------");
        System.out.println(family.prettyFormat());

        Menu.showMenu();
        Scanner in = new Scanner(System.in);

        while (true) {
            familyController.setAllFamilies(FileManager.loadData());
            String error = "Зробіть коректний вибір";
            String choice = "Введіть число";
            if (in.hasNextInt()) {
                int userResult = in.nextInt();
                in.nextLine();
                switch (userResult) {
                    case 1:
                        familyController.setAllFamilies(FileManager.loadTestData());
                        Menu.showMenu();
                        break;

                    case 2:
                        familyController.displayAllFamilies();
                        Menu.showMenu();
                        break;
                    case 3:
                        do {
                            System.out.println(choice);
                            if (in.hasNextInt()) {
                                int userResult3 = in.nextInt();
                                familyController.getFamiliesBiggerThan(userResult3);
                                Menu.showMenu();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        break;
                    case 4:
                        do {
                            System.out.println(choice);
                            if (in.hasNextInt()) {
                                int userResult4 = in.nextInt();
                                familyController.getFamiliesLessThan(userResult4);
                                Menu.showMenu();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        break;
                    case 5:
                        do {
                            System.out.println(choice);
                            if (in.hasNextInt()) {
                                int userResult5 = in.nextInt();
                                System.out.println(familyController.countFamiliesWithMemberNumber(userResult5) + " family/families");
                                Menu.showMenu();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        break;
                    case 6:
                        System.out.println("Створюємо нову родину");

                        System.out.println("Введіть ім'я матері");
                        String motherName = in.nextLine();
                        System.out.println("Введіть прізвище матері");
                        String motherSurname = in.nextLine();
                        System.out.println("Введіть рік народження матері");
                        int motherYear;
                        do {
                            if (in.hasNextInt()) {
                                motherYear = in.nextInt();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        in.nextLine();
                        String motherYearString = String.valueOf(motherYear);
                        System.out.println("Введіть місяць народження матері");
                        int motherMonth;
                        do {
                            if (in.hasNextInt()) {
                                motherMonth = in.nextInt();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        in.nextLine();
                        String motherMonthString = String.valueOf(motherMonth);
                        System.out.println("Введіть день народження матері");
                        int motherDay;
                        do {
                            if (in.hasNextInt()) {
                                motherDay = in.nextInt();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        in.nextLine();
                        String motherDayString = String.valueOf(motherDay);
                        StringBuilder motherBirthdate = new StringBuilder();
                        motherBirthdate.append(motherDayString).append("/").append(motherMonthString).append("/").append(motherYearString);
                        System.out.println("Введіть iq матері");
                        int motherIQ;
                        do {
                            if (in.hasNextInt()) {
                                motherIQ = in.nextInt();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        in.nextLine();
                        System.out.println("Введіть ім'я батька");
                        String fatherName = in.nextLine();
                        System.out.println("Введіть прізвище батька");
                        String fatherSurname = in.nextLine();
                        System.out.println("Введіть рік народження батька");
                        int fatherYear;
                        do {
                            if (in.hasNextInt()) {
                                fatherYear = in.nextInt();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        in.nextLine();
                        String fatherYearString = String.valueOf(fatherYear);
                        System.out.println("Введіть місяць народження батька");
                        int fatherMonth;
                        do {
                            if (in.hasNextInt()) {
                                fatherMonth = in.nextInt();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        in.nextLine();
                        String fatherMonthString = String.valueOf(fatherMonth);
                        System.out.println("Введіть день народження батька");
                        int fatherDay;
                        do {
                            if (in.hasNextInt()) {
                                fatherDay = in.nextInt();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        in.nextLine();
                        String fatherDayString = String.valueOf(fatherDay);
                        StringBuilder fatherBirthdate = new StringBuilder();
                        fatherBirthdate.append(fatherDayString).append("/").append(fatherMonthString).append("/").append(fatherYearString);
                        System.out.println("Введіть iq батька");
                        int fatherIQ;
                        do {
                            if (in.hasNextInt()) {
                                fatherIQ = in.nextInt();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        in.nextLine();

                        Woman mother5 = new Woman(motherName, motherSurname, String.valueOf(motherBirthdate), motherIQ);
                        Man father5 = new Man(fatherName, fatherSurname, String.valueOf(fatherBirthdate), fatherIQ);

                        familyController.createNewFamily(mother5, father5);
                        Menu.showMenu();
                        break;

                    case 7:
                        do {
                            System.out.println("Напишіть порядковий номер сім'ї (ID) для видалення");
                            if (in.hasNextInt()) {
                                int userResult7 = in.nextInt();
                                in.nextLine();
                                familyController.deleteFamilyByIndex(userResult7);
                                Menu.showMenu();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        break;

                    case 8:
                        do {
                            Menu.showMenuEditFamily();
                            if (in.hasNextInt()) {
                                int userResultEditFamily = in.nextInt();
                                in.nextLine();

                                switch (userResultEditFamily) {
                                    case 1:
                                        int familyId1;
                                        do {
                                            System.out.println("Напишіть порядковий номер сім'ї (ID)");
                                            if (in.hasNextInt()) {
                                                familyId1 = in.nextInt();
                                                in.nextLine();
                                                break;
                                            } else {
                                                System.out.println(choice);
                                                in.nextLine();
                                            }
                                        } while (true);

                                        System.out.println("Напишіть ім'я хлопчика");
                                        String boyName = in.nextLine();
                                        System.out.println("Напишіть ім'я дівчинки");
                                        String girlName = in.nextLine();
                                        familyController.bornChild(familyController.getFamilyById(familyId1), boyName, girlName);
                                        Menu.showMenu();
                                        break;
                                    case 2:
                                        int familyId2;
                                        do {
                                            System.out.println("Напишіть порядковий номер сім'ї (ID)");
                                            if (in.hasNextInt()) {
                                                familyId2 = in.nextInt();
                                                in.nextLine();
                                                break;
                                            } else {
                                                System.out.println(choice);
                                                in.nextLine();
                                            }
                                        } while (true);
                                        System.out.println("Напишіть ім'я дитини");
                                        String childName = in.nextLine();
                                        System.out.println("Напишіть прізвище дитини");
                                        String childSurname = in.nextLine();
                                        System.out.println("Введіть рік народження дитини");
                                        int childYear;
                                        do {
                                            if (in.hasNextInt()) {
                                                childYear = in.nextInt();
                                                break;
                                            } else {
                                                System.out.println(choice);
                                                in.nextLine();
                                            }
                                        } while (true);
                                        in.nextLine();
                                        String childYearString = String.valueOf(childYear);
                                        System.out.println("Введіть місяць народження дитини");
                                        int childMonth;
                                        do {
                                            if (in.hasNextInt()) {
                                                childMonth = in.nextInt();
                                                break;
                                            } else {
                                                System.out.println(choice);
                                                in.nextLine();
                                            }
                                        } while (true);
                                        in.nextLine();
                                        String childMonthString = String.valueOf(childMonth);
                                        System.out.println("Введіть день народження дитини");
                                        int childDay;
                                        do {
                                            if (in.hasNextInt()) {
                                                childDay = in.nextInt();
                                                break;
                                            } else {
                                                System.out.println(choice);
                                                in.nextLine();
                                            }
                                        } while (true);
                                        in.nextLine();
                                        String childDayString = String.valueOf(childDay);
                                        StringBuilder childBirthdate = new StringBuilder();
                                        childBirthdate.append(childDayString).append("/").append(childMonthString).append("/").append(childYearString);

                                        int childIQ;
                                        do {
                                            System.out.println("Введіть iq дитини");
                                            if (in.hasNextInt()) {
                                                childIQ = in.nextInt();
                                                in.nextLine();
                                                break;
                                            } else {
                                                System.out.println(choice);
                                                in.nextLine();
                                            }
                                        } while (true);
                                        Human child = new Human(childName, childSurname, String.valueOf(childBirthdate), childIQ);
                                        familyController.adoptChild(familyController.getFamilyById(familyId2), child);
                                        Menu.showMenu();
                                        break;
                                    case 3:
                                        Menu.showMenu();
                                        break;
                                    default:
                                        System.out.println(error);
                                        break;
                                }
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        break;

                    case 9:
                        int userResult9;
                        do {
                            System.out.println("Введіть вік для видалення всіх дітей старше даного віку:");
                            if (in.hasNextInt()) {
                                userResult9 = in.nextInt();
                                familyController.deleteAllChildrenOlderThen(userResult9);
                                Menu.showMenu();
                                break;
                            } else {
                                System.out.println(choice);
                                in.nextLine();
                            }
                        } while (true);
                        break;

                    default:
                        System.out.println(error);
                        Menu.showMenu();
                }
            } else {
                String input = in.next().toLowerCase();
                if (input.equals("exit")){
                    FileManager.writeData(familyController.getAllFamilies());
                    return;
                }
                else {
                    System.out.println(error);
                }
            }

        }

    }
}
