package main.com.hw_12;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LoggerService {
    public static final String application = "Data/application.log";
    public static String getFormattedDateTime() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime localDateTime = LocalDateTime.now();
        return dateTimeFormatter.format(localDateTime);
    }

    public static void writeDataLong(String message) {
        String text = message + "\n";
        Path path = Paths.get(application);
        try {
            if (!Files.exists(path)) {
                Files.createFile(path);
            }
            Files.write(path, text.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void info(String message) {
        String info = getFormattedDateTime() + " [DEBUG] " + message;
        System.out.println(info);
        writeDataLong(info);
    }

    public static void error(String message) {
        String info = getFormattedDateTime() + " [ERROR] " + message;
        System.out.println(info);
        writeDataLong(info);
    }

}
