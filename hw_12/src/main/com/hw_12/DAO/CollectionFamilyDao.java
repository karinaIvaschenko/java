package main.com.hw_12.DAO;

import main.com.hw_12.Family;
import main.com.hw_12.LoggerService;


import java.util.*;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList;

    public CollectionFamilyDao() {
        this.familyList = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        LoggerService.info("Отримання списка сімей");
        return this.familyList;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        try {
            LoggerService.info("Отримання сімʼї за індексом");
            return this.familyList.get(index);
        } catch (Exception e) {
            LoggerService.error("Помилка при отриманні сімʼї за індексом" + e.getMessage());
            throw e;
        }
    }

    @Override
    public boolean deleteFamily(int index) {
        try {
            LoggerService.info("Bидалення сім'ї за індексом");
            if (index >= 0 && index < this.familyList.size()) {
                this.familyList.remove(index);
                return true;
            }
        } catch (Exception e) {
            LoggerService.error("Помилка при видаленні сімʼї за індексом" + e.getMessage());
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        try {
            LoggerService.info("Bидалення сім'ї за типом");
            if (familyList.size() > 0) {
                this.familyList.remove(family);
                return true;
            }
        }catch (Exception e){
            LoggerService.error("Помилка при видаленні сімʼї за типом" + e.getMessage());
        }

        return false;
    }

    @Override
    public void saveFamily(Family family) {
        try {
            LoggerService.info("Збереження сімʼї");
            if (this.familyList.contains(family)) {
                int index = familyList.indexOf(family);
                this.familyList.set(index, family);
            } else {
                familyList.add(family);
            }
        }catch (Exception e){
            LoggerService.error("Помилка при збереженні сімʼї" + e.getMessage());
        }
    }

    @Override
    public void setAllFamilies(List<Family> familyList) {
        LoggerService.info("Збереження списку сімей");
        this.familyList = familyList;
    }
}