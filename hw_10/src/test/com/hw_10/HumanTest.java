package test.com.hw_10;

import main.com.hw_10.DayOfWeek;
import main.com.hw_10.Human;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testToString() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "go home");
        schedule.put(DayOfWeek.TUESDAY, "go to gym");
        schedule.put(DayOfWeek.WEDNESDAY, "SPA");
        schedule.put(DayOfWeek.THURSDAY, "go home");
        schedule.put(DayOfWeek.FRIDAY, "visit friends");
        schedule.put(DayOfWeek.SATURDAY, "film");
        schedule.put(DayOfWeek.SUNDAY, "zoo");

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        String mother1 = "Human{name= 'Kari', surname= 'Ivaschenko', year= 1999, iq= 100schedule= {SUNDAY=zoo, FRIDAY=visit friends, SATURDAY=film, WEDNESDAY=SPA, MONDAY=go home, TUESDAY=go to gym, THURSDAY=go home}}}";
        String mother2 = "Human{name= 'Karina', surname= 'Ivaschenko', year= 1999, iq= 100schedule= [[MONDAY, go home], [TUESDAY, go to gym], [WEDNESDAY, SPA], [THURSDAY, go home], [FRIDAY, visit friends], [SATURDAY, film], [SUNDAY, zoo]]}}";
        assertEquals(mother1, mother.toString(), "Ok");
        assertNotEquals(mother2, mother.toString(), "Not ok");
    }

    @Test
    void testEquals() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "go home");
        schedule.put(DayOfWeek.TUESDAY, "go to gym");
        schedule.put(DayOfWeek.WEDNESDAY, "SPA");
        schedule.put(DayOfWeek.THURSDAY, "go home");
        schedule.put(DayOfWeek.FRIDAY, "visit friends");
        schedule.put(DayOfWeek.SATURDAY, "film");
        schedule.put(DayOfWeek.SUNDAY, "zoo");

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        Human mother1 = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        Human mother2 = new Human("Karina", "Ivaschenko", 1999, 100, schedule);
        assertEquals(mother, mother1, "Ok");
        assertNotEquals(mother, mother2, "Not Ok");
    }

    @Test
    void testHashCode() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "go home");
        schedule.put(DayOfWeek.TUESDAY, "go to gym");
        schedule.put(DayOfWeek.WEDNESDAY, "SPA");
        schedule.put(DayOfWeek.THURSDAY, "go home");
        schedule.put(DayOfWeek.FRIDAY, "visit friends");
        schedule.put(DayOfWeek.SATURDAY, "film");
        schedule.put(DayOfWeek.SUNDAY, "zoo");

        Human mother = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        Human mother1 = new Human("Kari", "Ivaschenko", 1999, 100, schedule);
        Human mother2 = new Human("Karina", "Ivaschenko", 1999, 100, schedule);
        assertEquals(mother.hashCode(), mother1.hashCode(), "Ok");
        assertNotEquals(mother.hashCode(), mother2.hashCode(), "Not Ok");
    }
}