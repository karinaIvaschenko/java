package main.com.hw_10.Services;

import main.com.hw_10.*;
import main.com.hw_10.DAO.CollectionFamilyDao;

import java.time.Year;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FamilyService {
    private CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return collectionFamilyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> allFamilies = getAllFamilies();
        System.out.println("All families:");
        List<String> result = allFamilies.stream().map(family -> family.toString()).collect(Collectors.toList());
        IntStream.range(0, allFamilies.size())
                .forEach(index -> System.out.println(index+1 + ". " + result.get(index)));
    }

    public List<Family> getFamiliesBiggerThan(int quantity) {
        return getAllFamilies().stream()
                .filter(family -> family.countFamily() > quantity)
                .peek(System.out::println)
                .toList();
    }

    public List<Family> getFamiliesLessThan(int quantity) {
        return getAllFamilies().stream()
                .filter(family -> family.countFamily() < quantity)
                .peek(System.out::println)
                .toList();
    }

    public int countFamiliesWithMemberNumber(int quantity) {
        return (int) getAllFamilies().stream()
                .filter(family -> family.countFamily() == quantity)
                .peek(System.out::println)
                .count();
    }

    public void createNewFamily(Human human1, Human human2) {
        Family family = new Family(human1, human2);
        collectionFamilyDao.saveFamily(family);
    }

    public Family bornChild(Family family, String boyName, String girlName) {
        String childName = Math.random() < 0.5 ? boyName : girlName;
        Human child;
        if (childName.equals(boyName)) {
            child = new Man();
        } else {
            child = new Woman();
        }
        child.setFamily(family);
        child.setSurname(family.getFather().getSurname());
        child.setName(childName);
        family.addChild(child);
        return family;
    }

    public Family adoptChild(Family family, Human human) {
        family.addChild(human);
        collectionFamilyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        Year currentYear = Year.now();
        getAllFamilies().stream()
                .forEach(family -> {family.setChildren(family.getChildren()
                        .stream().filter(human -> {
                            int birthYear = human.calculateBirthdayZonedDateTime(human.getBirthDate()).getYear();
                            int ageOfHuman = currentYear.getValue() - birthYear;
                            return ageOfHuman <= age;
                        }).toList()
                );
                    collectionFamilyDao.saveFamily(family);
                });
        }

    public int count() {
        System.out.println("The quantity of families is " + collectionFamilyDao.getAllFamilies().size());
        return getAllFamilies().size();
    }

    public Set<Pet> getPets(int index) {
        Family family = collectionFamilyDao.getFamilyByIndex(index);
        System.out.println("This family has pets: " + family.getPets());
        return family.getPets();
    }

    public void addPet(int familyIndex, Pet pet) {
        if (familyIndex >= 0 && familyIndex < getAllFamilies().size()) {
            getAllFamilies().get(familyIndex).addPet(pet);
        }
    }

    public boolean deleteFamilyByIndex(int index) {
        return collectionFamilyDao.deleteFamily(index);
    }

    public boolean deleteFamilyByFamily(Family family) {
        return collectionFamilyDao.deleteFamily(family);
    }

    public Family getFamilyById(int index) {
        System.out.println(collectionFamilyDao.getFamilyByIndex(index));
        return collectionFamilyDao.getFamilyByIndex(index);
    }
}