package main.com.homew_7;

public interface HumanCreator {
     void bornChild(Human child);
}
